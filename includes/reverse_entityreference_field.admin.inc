<?php

/**
 * Page callback, returns a list off all entityreferences.
 */
function reverse_entityreference_field_reports_page() {
  $header = array(
    array('data' => t('Enabled')),
    array('data' => t('Reversed entityreference field')),
    array('data' => t('Source (entity type, bundle)')),
    array('data' => t('Target (entity type, bundle)')),
    array('data' => t('Operations')),
  );

  $rows = array();
  foreach (reverse_entityreference_field_instances() as $instance) {
    $enabled = t('No');
    $field = '';
    $settings = array();

    $target_bundle = reset($instance['settings']['handler_settings']['target_bundles']);
    $source_path = _field_ui_bundle_admin_path($instance['entity_type'], $instance['bundle']);
    $target_path = _field_ui_bundle_admin_path($instance['settings']['target_type'], $target_bundle);

    if ($instance['enabled']) {
      $enabled = t('Yes');
      $field = $instance['label'];
      $settings[] = l(t('Field settings'), $target_path . '/fields/' . $instance['settings']['target_field_name']);
      $settings[] = l(t('Manage display'), $source_path . '/display');
    }

    $rows[] = array(
      array('data' => $enabled),
      array('data' => $field),
      array('data' => l(t('@entity_type, @bundle', array(
        '@entity_type' => $instance['entity_type'],
        '@bundle' => $instance['bundle'],
      )), $source_path . '/fields')),
      array('data' => l(t('@entity_type, @bundle', array(
        '@entity_type' => $instance['settings']['target_type'],
        '@bundle' => $target_bundle,
      )), $target_path . '/fields')),
      array('data' => implode(', ', $settings)),
    );
  }

  return theme('table', array("header" => $header, "rows" => $rows, 'empty' => t('No entity references found.')));
}
